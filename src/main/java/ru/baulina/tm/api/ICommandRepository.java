package ru.baulina.tm.api;

import ru.baulina.tm.model.TerminalCommand;

import java.util.Arrays;

public interface ICommandRepository {

    String[] getCommands(TerminalCommand... values);

    String[] getArgs(TerminalCommand... values);

    TerminalCommand[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();

}
