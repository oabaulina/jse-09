package ru.baulina.tm;

import ru.baulina.tm.constant.ArgumentConst;
import ru.baulina.tm.constant.CommandConst;
import ru.baulina.tm.model.TerminalCommand;
import ru.baulina.tm.repository.CommandRepository;
import ru.baulina.tm.util.NumberUtil;

import java.util.Arrays;
import java.util.Scanner;

public class App {

    public static final CommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(final String[] args) {
        displayWelcome();
        if (parseCommands(args) || parseArgs(args)) exit();
        process();
    }

    private static void process() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!CommandConst.EXIT.equals(command)) {
            command = scanner.nextLine();
            parseCommand(command);
        }
    }

    private static boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        return parseArg(arg);
    }

    private static boolean parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return false;
        switch (arg) {
            case ArgumentConst.HELP:
                showHelp();
                return true;
            case ArgumentConst.ABOUT:
                showAbout();
                return true;
            case ArgumentConst.VERSION:
                showVersion();
                return true;
            case ArgumentConst.COMMANDS:
                showCommands();
                return true;
            case ArgumentConst.ARGUMENTS:
                showArguments();
                return true;
            case ArgumentConst.INFO:
                showInfo();
                return true;
            default:
        }
        return false;
    }

    private static boolean parseCommands(final String[] commands) {
        if (commands == null || commands.length == 0) return false;
        final String command = commands[0];
        return parseCommand(command);
    }

    private static boolean parseCommand(final String command) {
        if (command == null || command.isEmpty()) return false;
        switch (command.toLowerCase()) {
            case CommandConst.HELP:
                showHelp();
                return true;
            case CommandConst.ABOUT:
                showAbout();
                return true;
            case CommandConst.VERSION:
                showVersion();
                return true;
            case CommandConst.COMMANDS:
                showCommands();
                return true;
            case CommandConst.ARGUMENTS:
                showArguments();
                return true;
            case CommandConst.INFO:
                showInfo();
                return true;
            case CommandConst.EXIT:
                exit();
                return true;
            default:
        }
        return false;
    }

    private static void displayWelcome() {
        System.out.println("** Welcome to task manager **");
        System.out.println();
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        final TerminalCommand[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final  TerminalCommand command: commands) System.out.println(command);
        System.out.println();
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.9");
        System.out.println();
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Olga Baulina");
        System.out.println("golovolomkacom@gmail.com");
        System.out.println();
    }

    private static void showCommands() {
        final String[] commands = COMMAND_REPOSITORY.getCommands();
        System.out.println(Arrays.toString(commands));
        System.out.println();
    }

    private static void showArguments() {
        final String[] arguments = COMMAND_REPOSITORY.getArgs();
        System.out.println(Arrays.toString(arguments));
        System.out.println();
    }

    private static void showInfo() {
        System.out.println("[INFO]");

        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);

        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        System.out.println("Used memory by JVM: " + usedMemoryFormat);
        System.out.println();

    }

    private static void exit() {
        System.exit(0);
    }

}


